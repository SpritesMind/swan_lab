#include <swan.h>
#include <i86.h>



inrom_char serial_title [] = "Serial Tester";
inrom_char serial_item1 [] = "Serial Sync";
inrom_char serial_item2 [] = "Serial Async";


inrom_char ser_X1 [] = "X1 to set 9600bps";
inrom_char ser_X2 [] = "X2 to set 38400bps";
//inrom_char seren_Y1 [] = "Y1 enable int";
//inrom_char seren_Y2 [] = "Y2 disable int";
inrom_char ser_StartHello [] = "START to send Hello string";



inrom_char ser_status [] = "Status:";
inrom_char serout_error [] = "Send error";
inrom_char serout_ok [] =    "Sending...";
inrom_char seren_int    [] = "Interrupt ";
inrom_char seren_intOK  [] = "received";
inrom_char seren_intWait[] = "waiting ";

inrom_char serout_hello[] = "Hello";

u8 col, line;
u8 helloIndex;

#define no_error 	VDP_fillTileMap(16,3,10, 1, VDP_BG_SEG, TILE(0, PAL_MONO_TEXT,NO_HFLIP,NO_VFLIP)); //erase error text


void ser_screenInit()
{

	VDP_printText(1, 3, VDP_BG_SEG, (inrom_char *) ser_status);
	
	no_error;
	
	VDP_printText(1, SCREEN_HEIGHT-4, VDP_BG_SEG, (inrom_char *) ser_StartHello);
	VDP_printText(1, SCREEN_HEIGHT-3, VDP_BG_SEG, (inrom_char *) ser_X1);
	VDP_printText(1, SCREEN_HEIGHT-2, VDP_BG_SEG, (inrom_char *) ser_X2);
	
	SERIAL_open(FALSE); //9600bauds
	
	col = 0;
	line = 5;
	helloIndex = 0xFF;

	
	JOY_reset();
}


void sendByte()
{
	if (helloIndex == 0xFF) 	return;
	
	if (SERIAL_send(serout_hello[helloIndex]))	
	{
		if ((++helloIndex) >= 5)
		{
			helloIndex = 0xFF;
			no_error;
		}
		else
		{
			VDP_printText(16, 3, VDP_BG_SEG, (inrom_char *) serout_ok);
		}
	}
	else
	{
		VDP_printText(16, 3, VDP_BG_SEG, (inrom_char *) serout_error);
	}
}

void receivedByte()
{
	u8  value = 0;
	//we coudl receive while sending ?
	if ( SERIAL_receive(&value) )
	{
		if (value < 0x20) value = '.';
		else if (value > 0x7E) value = '.';
		
		VDP_setTile(col, line, VDP_BG_SEG, TILE( (value-0x20+FONT_TILE_INDEX), PAL_MONO_TEXT, NO_HFLIP,NO_VFLIP));
		col++;
		if (col == SCREEN_WIDTH)
		{
			col = 0;
			line++;
			if (line == SCREEN_HEIGHT-5)	line = 5;
			VDP_fillTileMap(0,line,SCREEN_WIDTH, 1, VDP_BG_SEG, TILE(0, PAL_MONO_TEXT,NO_HFLIP,NO_VFLIP));
		}
	}
}

////////////////////////////////////////////////////////
///////////// SERIAL ASYC 
////////////////////////////////////////////////////////
void serialAsync_tester( )
{
	VDP_printText((SCREEN_WIDTH-17)/2, 1, VDP_BG_SEG, (inrom_char *) serial_item2);
	
	ser_screenInit();
	
	SYS_setInterruptMask(MASK_INT_VBLANK | MASK_INT_SER_TX | MASK_INT_SER_RX ); //vsync and tx
	SYS_setInterruptHandler(INT_SER_TX, &sendByte);
	SYS_setInterruptHandler(INT_SER_RX, &receivedByte);
	

	while (1)
	{
		wait_vsync();
		
		VDP_printHexa(10, 3, VDP_BG_SEG, SERIAL_getStatus(), 2);
		VDP_printHexa(13, 3, VDP_BG_SEG, SYS_getInterruptMask(), 2);
		
		JOY_update();
		if (joy_change == 0)	continue;
		
		if (joy_current == (JOY_START | JOY_A))	break; 
			
		if (joy_pressed & JOY_X1)
		{
			helloIndex = 0xFF;
			no_error;
			SERIAL_open(FALSE);
		}
		else if (joy_released & JOY_X2)
		{
			helloIndex = 0xFF;
			no_error;
			SERIAL_open(TRUE);
		}
		else if (joy_released & JOY_START)
		{
			helloIndex = 0;
			
			//try to send first byte, else wait for RX int
			if (SERIAL_send(serout_hello[helloIndex]))	helloIndex++;
		}
	}
	
	//SYS_reset()
	// or
	SYS_setInterruptMask(MASK_INT_VBLANK); //vsync  only
	SYS_setInterruptHandler(INT_SER_TX, NULL); //clean
	SYS_setInterruptHandler(INT_SER_RX, NULL); //clean
	
	SERIAL_close();
}


////////////////////////////////////////////////////////
///////////// SERIAL Synchronous 
////////////////////////////////////////////////////////
void serialSync_tester( )
{
	ser_screenInit();

	VDP_printText((SCREEN_WIDTH-9)/2, 1, VDP_BG_SEG, (inrom_char *) serial_item1);
	
	while (1)
	{
		wait_vsync(); //1 char per frame ( Anchor Field Z do it per line ;) )
		
		VDP_printHexa(10, 3, VDP_BG_SEG, SERIAL_getStatus(), 2);
		
		if (helloIndex != 0xFF)	sendByte();
		receivedByte();
		
		JOY_update();
		
		if (joy_change == 0)	continue;
		
		if (joy_current == (JOY_START | JOY_A))	break; 
		
		if (joy_pressed & JOY_START)
		{
			helloIndex = 0;
		}
		else if (joy_pressed & JOY_X1)
		{
			helloIndex = 0xFF;
			no_error;
			SERIAL_open(FALSE);
		}
		else if (joy_released & JOY_X2)
		{
			helloIndex = 0xFF;
			no_error;
			SERIAL_open(TRUE);
		}
	}
	
	SERIAL_close();
}



////////////////////////////////////////////////////////
///////////// MENU 
////////////////////////////////////////////////////////



#define	MENU_SELECTOR	('*'-32)
u8 serial_menu()
{
	u8 posx, posy;
	u8 spriteIndex;
	
	VDP_clearBG();
	VDP_clearFG();	
	SPRITE_reset();
	
	VDP_printText((SCREEN_WIDTH-17)/2, 1, VDP_BG_SEG, (inrom_char *) serial_title);

	VDP_printText(2,  5, VDP_BG_SEG, (inrom_char *) serial_item1);
	VDP_printText(2,  7, VDP_BG_SEG, (inrom_char *) serial_item2);
	
	
	
	spriteIndex = SPRITE_add( SPRITE(MENU_SELECTOR, PAL_MONO_TEXT, SPRITE_WINDOWS_IN, SPRITE_PRIORITY_NORMAL, NO_HFLIP, NO_VFLIP) );
	posx = 4; //1*8;
	posy = 5*8;
	
	JOY_reset();
	while (1)
	{		
		wait_vsync();
		
		JOY_update();

		SPRITE_setPosition(spriteIndex, posx, posy);
		SPRITE_flush();
		
		if (joy_change == 0)	continue;
		
		if (joy_current == (JOY_START | JOY_A))	return 0xFF; 
		
		if (joy_pressed & JOY_A) 	return  ((posy - 5*8)/16);
		
		if (joy_pressed & JOY_X1)		posy-=16;
		else if (joy_pressed & JOY_X3)	posy+=16;
		
		if (posy <  5*8)		posy = 5*8;
		if (posy >  7*8)		posy = 7*8;
	}
}


void serial_tester( )
{	
	while(1)
	{
		u8 choice = serial_menu();

		VDP_clearBG();
		VDP_clearFG();
		SPRITE_reset( );

	
		switch(choice)	
		{
			case 0: 
				serialSync_tester();
				break;
				
			case 1:	
				serialAsync_tester();
				break;
				
			case 0xFF:
			default:
				return;
				break;
		}

	}
}
