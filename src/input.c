#include <swan.h>
#include <i86.h>



////////////////////////////////////////////////////////
///////////// JOY 
////////////////////////////////////////////////////////

inrom_char input[] = "Joy Tester";
inrom_char input_current[] = "CURRENT";
inrom_char input_pressed[] = "PRESSED";
inrom_char input_released[] = "RELEASED";
inrom_char input_start[] = "START";
inrom_char input_X1[] = "X1";
inrom_char input_X2[] = "X2";
inrom_char input_X3[] = "X3";
inrom_char input_X4[] = "X4";
inrom_char input_Y1[] = "Y1";
inrom_char input_Y2[] = "Y2";
inrom_char input_Y3[] = "Y3";
inrom_char input_Y4[] = "Y4";
inrom_char input_A[] = "A";
inrom_char input_B[] = "B";


#define	CURRENT		1
#define	PRESSED		10
#define RELEASED	19
#define INPUT_Y		6
void joy_tester( )
{
	VDP_printText((SCREEN_WIDTH-12)/2, 1, VDP_BG_SEG, (inrom_char *) input);

	VDP_printText(CURRENT, INPUT_Y-2, VDP_BG_SEG, (inrom_char *) input_current);
	VDP_printText(PRESSED, INPUT_Y-2, VDP_BG_SEG, (inrom_char *) input_pressed);
	VDP_printText(RELEASED, INPUT_Y-2, VDP_BG_SEG, (inrom_char *) input_released);
	

	while (1)
	{
		
		wait_vsync();
		
		JOY_update();
		if (joy_change == 0)	continue;
		
		if (joy_current == (JOY_START | JOY_A))	break; 
		
		VDP_clearFG();
		
		//START
		if (joy_current & JOY_START)
			VDP_printText(CURRENT, INPUT_Y+0, VDP_FG_SEG, (inrom_char *) input_start);
		if (joy_pressed & JOY_START)
			VDP_printText(PRESSED, INPUT_Y+0, VDP_FG_SEG, (inrom_char *) input_start);
		if (joy_released & JOY_START)
			VDP_printText(RELEASED, INPUT_Y+0, VDP_FG_SEG, (inrom_char *) input_start);
		
		//A
		if (joy_current & JOY_A)
			VDP_printText(CURRENT, INPUT_Y+1, VDP_FG_SEG, (inrom_char *) input_A);
		if (joy_pressed & JOY_A)
			VDP_printText(PRESSED, INPUT_Y+1, VDP_FG_SEG, (inrom_char *) input_A);
		if (joy_released & JOY_A)
			VDP_printText(RELEASED, INPUT_Y+1, VDP_FG_SEG, (inrom_char *) input_A);
		
		//B
		if (joy_current & JOY_B)
			VDP_printText(CURRENT, INPUT_Y+2, VDP_FG_SEG, (inrom_char *) input_B);
		if (joy_pressed & JOY_B)
			VDP_printText(PRESSED, INPUT_Y+2, VDP_FG_SEG, (inrom_char *) input_B);
		if (joy_released & JOY_B)
			VDP_printText(RELEASED, INPUT_Y+2, VDP_FG_SEG, (inrom_char *) input_B);
		
		//X1
		if (joy_current & JOY_X1)
			VDP_printText(CURRENT, INPUT_Y+3, VDP_FG_SEG, (inrom_char *) input_X1);
		if (joy_pressed & JOY_X1)
			VDP_printText(PRESSED, INPUT_Y+3, VDP_FG_SEG, (inrom_char *) input_X1);
		if (joy_released & JOY_X1)
			VDP_printText(RELEASED, INPUT_Y+3, VDP_FG_SEG, (inrom_char *) input_X1);

		//X2
		if (joy_current & JOY_X2)
			VDP_printText(CURRENT, INPUT_Y+4, VDP_FG_SEG, (inrom_char *) input_X2);
		if (joy_pressed & JOY_X2)
			VDP_printText(PRESSED, INPUT_Y+4, VDP_FG_SEG, (inrom_char *) input_X2);
		if (joy_released & JOY_X2)
			VDP_printText(RELEASED, INPUT_Y+4, VDP_FG_SEG, (inrom_char *) input_X2);
			
		//X3
		if (joy_current & JOY_X3)
			VDP_printText(CURRENT, INPUT_Y+5, VDP_FG_SEG, (inrom_char *) input_X3);
		if (joy_pressed & JOY_X3)
			VDP_printText(PRESSED, INPUT_Y+5, VDP_FG_SEG, (inrom_char *) input_X3);
		if (joy_released & JOY_X3)
			VDP_printText(RELEASED, INPUT_Y+5, VDP_FG_SEG, (inrom_char *) input_X3);
			
		//X4
		if (joy_current & JOY_X4)
			VDP_printText(CURRENT, INPUT_Y+6, VDP_FG_SEG, (inrom_char *) input_X4);
		if (joy_pressed & JOY_X4)
			VDP_printText(PRESSED, INPUT_Y+6, VDP_FG_SEG, (inrom_char *) input_X4);
		if (joy_released & JOY_X4)
			VDP_printText(RELEASED, INPUT_Y+6, VDP_FG_SEG, (inrom_char *) input_X4);

		//Y1
		if (joy_current & JOY_Y1)
			VDP_printText(CURRENT, INPUT_Y+7, VDP_FG_SEG, (inrom_char *) input_Y1);
		if (joy_pressed & JOY_Y1)
			VDP_printText(PRESSED, INPUT_Y+7, VDP_FG_SEG, (inrom_char *) input_Y1);
		if (joy_released & JOY_Y1)
			VDP_printText(RELEASED, INPUT_Y+7, VDP_FG_SEG, (inrom_char *) input_Y1);

		//Y2
		if (joy_current & JOY_Y2)
			VDP_printText(CURRENT, INPUT_Y+8, VDP_FG_SEG, (inrom_char *) input_Y2);
		if (joy_pressed & JOY_Y2)
			VDP_printText(PRESSED, INPUT_Y+8, VDP_FG_SEG, (inrom_char *) input_Y2);
		if (joy_released & JOY_Y2)
			VDP_printText(RELEASED, INPUT_Y+8, VDP_FG_SEG, (inrom_char *) input_Y2);

		//Y3
		if (joy_current & JOY_Y3)
			VDP_printText(CURRENT, INPUT_Y+9, VDP_FG_SEG, (inrom_char *) input_Y3);
		if (joy_pressed & JOY_Y3)
			VDP_printText(PRESSED, INPUT_Y+9, VDP_FG_SEG, (inrom_char *) input_Y3);
		if (joy_released & JOY_Y3)
			VDP_printText(RELEASED, INPUT_Y+9, VDP_FG_SEG, (inrom_char *) input_Y3);

		//Y4
		if (joy_current & JOY_Y4)
			VDP_printText(CURRENT, INPUT_Y+10, VDP_FG_SEG, (inrom_char *) input_Y4);
		if (joy_pressed & JOY_Y4)
			VDP_printText(PRESSED, INPUT_Y+10, VDP_FG_SEG, (inrom_char *) input_Y4);
		if (joy_released & JOY_Y4)
			VDP_printText(RELEASED, INPUT_Y+10, VDP_FG_SEG, (inrom_char *) input_Y4);

	}
}



////////////////////////////////////////////////////////
///////////// MENU 
////////////////////////////////////////////////////////

inrom_char input_title [] = "Input Tester";
inrom_char input_item1 [] = "Joypad";
inrom_char input_item2 [] = "x GPIO";


#define	MENU_SELECTOR	('*'-32)
u8 input_menu()
{
	u8 posx, posy;
	u8 spriteIndex;
	
	VDP_printText((SCREEN_WIDTH-17)/2, 1, VDP_BG_SEG, (inrom_char *) input_title);

	VDP_printText(2, 5, VDP_BG_SEG, (inrom_char *) input_item1);
	VDP_printText(2, 7, VDP_BG_SEG, (inrom_char *) input_item2);
	
	
	
	spriteIndex = SPRITE_add( SPRITE(MENU_SELECTOR, PAL_MONO_TEXT, SPRITE_WINDOWS_IN, SPRITE_PRIORITY_NORMAL, NO_HFLIP, NO_VFLIP) );
	posx = 4; //1*8;
	posy = 5*8;

	
	JOY_reset();
	while (1)
	{		
		wait_vsync();
		
		SPRITE_setPosition(spriteIndex, posx, posy);
		SPRITE_flush();
		
		JOY_update();
		if (joy_change == 0)	continue;

		if (joy_current == (JOY_START | JOY_A))	return 0xFF; 
		
		
		
		
		if (joy_pressed & JOY_A) 	return  ((posy - 5*8)/16);
		
		if (joy_pressed & JOY_X1)		posy-=16;
		else if (joy_pressed & JOY_X3)	posy+=16;
		
		if (posy < 5*8)	posy = 5*8;
		if (posy > 7*8)	posy = 7*8;
	}
}


void input_tester( )
{	
	while(1)
	{
		u8 choice = input_menu();

		VDP_clearBG();
		VDP_clearFG();
		SPRITE_reset( );

	
		switch(choice)	
		{
			case 0: 
				joy_tester();
				break;
				
			case 1:	
				//TODO
				break;
				
			case 0xFF:
			default:
				return;
				break;
		}

		VDP_clearBG();
		VDP_clearFG();	
		SPRITE_reset( );
	}
}
