#include <swan.h>
#include <i86.h>






////////////////////////////////////////////////////////
///////////// TEXT 
////////////////////////////////////////////////////////
inrom_char hello[] = "HELLO  WORLD";
inrom_char hello_thanks1[] = "Thanks to Gozilu";
inrom_char hello_thanks1A[] = "Sorry for the spams!";
inrom_char hello_thanks2[] = "Thanks to trap0xf";
inrom_char hello_thanks2A[] = "WONDER repo was gold";


void video_text()
{
	//this doesn't work -> watcom but it on DATA
	//const char __far hello_thanks2B[] = "WONDER repo was gold";

	u8 posy = 1;


	VDP_printText((SCREEN_WIDTH-12)/2, posy, VDP_BG_SEG, (inrom_char *) hello);

	VDP_printText(1, posy+ 5, VDP_BG_SEG, (inrom_char *) hello_thanks1);
	VDP_printText(3, posy+ 6, VDP_BG_SEG, (inrom_char *) hello_thanks1A);
	VDP_printText(1, posy+ 9, VDP_BG_SEG, (inrom_char *) hello_thanks2);
	VDP_printText(3, posy+10, VDP_BG_SEG, (inrom_char *) hello_thanks2A);
	
	
	VDP_printNumber(1, SCREEN_HEIGHT-2, VDP_BG_SEG, 1024, 6);
	VDP_printHexa(SCREEN_WIDTH-6-1, SCREEN_HEIGHT-2, VDP_BG_SEG, 1024, 6);

	
	//this doesn't work -> watcom but it on DATA
	//VDP_printText(3, posy+11, VDP_BG_SEG, (const char __far *) "WONDER repo was gold");

	
	JOY_reset();
	while ( 1 )
	{
		wait_vsync();
		
		JOY_update();
		if (joy_change == 0)	continue;

		if (joy_current == (JOY_START | JOY_A))	break; 
	}
}


////////////////////////////////////////////////////////
///////////// TILES 
////////////////////////////////////////////////////////
inrom_char tiles[] = "Tiles";
inrom_char tiles_packed[] = "Press B to switch to packed mode";
inrom_char tiles_planar[] = "Press B to switch to planar mode";

extern bitmap_rsc kyo_ws_planar;
extern bitmap_rsc kyo_ws_packed;

void video_tiles( )
{
	u8 mode = 0;	
	bitmap *tilesPtr;
	
	//BANK_setDataBank(9);
	VDP_printText((SCREEN_WIDTH-6)/2, 1, VDP_BG_SEG, (inrom_char *) tiles);
	VDP_printText( (SCREEN_WIDTH-25)/2, SCREEN_HEIGHT-1, VDP_BG_SEG, (inrom_char *) tiles_packed);

	//PAL_MONO_2 no transparency so full 4 colors
	tilesPtr = GET_BITMAP(&kyo_ws_planar);
	VDP_loadTile( (inrom_u16 *) GET_DATA(tilesPtr->data_offset), tilesPtr->width*tilesPtr->height, USER_TILE_INDEX);
	VDP_setPalIndexes(PAL_MONO_2, (u8 __far *) GET_DATA(tilesPtr->pal_offset));

	

	VDP_drawTileMap((SCREEN_WIDTH-tilesPtr->width)/2, (SCREEN_HEIGHT-tilesPtr->height)/2, tilesPtr->width, tilesPtr->height, VDP_BG_SEG, TILE(USER_TILE_INDEX,PAL_MONO_2,NO_HFLIP,NO_VFLIP));

	
	JOY_reset();
	while (1)
	{
		wait_vsync();
		
		JOY_update();
		if (joy_change == 0)	continue;
		
		if (joy_current == (JOY_START | JOY_A))	break; 
		
		if (joy_pressed & JOY_B)		
		{
			mode++;
			if (mode&1)
			{
				VDP_setTilePackedMode(TRUE);
				
				tilesPtr = GET_BITMAP(&kyo_ws_packed);
				VDP_loadTile( (inrom_u16 *) GET_DATA(tilesPtr->data_offset), tilesPtr->width*tilesPtr->height, USER_TILE_INDEX);
				VDP_drawTileMap((SCREEN_WIDTH-tilesPtr->width)/2, (SCREEN_HEIGHT-tilesPtr->height)/2, tilesPtr->width, tilesPtr->height, VDP_BG_SEG, TILE(USER_TILE_INDEX,PAL_MONO_2,NO_HFLIP,NO_VFLIP));
				
				VDP_printText( (SCREEN_WIDTH-25)/2, SCREEN_HEIGHT-1, VDP_BG_SEG, (inrom_char *) tiles_planar);
			}	
			else
			{
				VDP_setTilePackedMode(FALSE);
				
				tilesPtr = GET_BITMAP(&kyo_ws_planar);
				VDP_loadTile( (inrom_u16 *) GET_DATA(tilesPtr->data_offset), tilesPtr->width*tilesPtr->height, USER_TILE_INDEX);
				VDP_drawTileMap((SCREEN_WIDTH-tilesPtr->width)/2, (SCREEN_HEIGHT-tilesPtr->height)/2, tilesPtr->width, tilesPtr->height, VDP_BG_SEG, TILE(USER_TILE_INDEX,PAL_MONO_2,NO_HFLIP,NO_VFLIP));

				VDP_printText( (SCREEN_WIDTH-25)/2, SCREEN_HEIGHT-1, VDP_BG_SEG, (inrom_char *) tiles_packed);
			}	

		}	
	}
}



////////////////////////////////////////////////////////
///////////// SPRITE 
////////////////////////////////////////////////////////
inrom_char sprite[] = "Sprites";
extern bitmap_rsc biker_ws;

inrom_char bikerOff[] = {
//	x,y
	0,0,
	8,0,
	0,8,
	8,8
};


#define CARRET_TILE	64

void video_sprites( )
{
	u8 spriteIndex, bikerIndex;
	u8 posx,posy;	
	s8 dirx, diry;
	bitmap *bikerPtr;
	
	inrom_char *offset;


	//convert to code:bikerOff
	offset = (inrom_char *) GET_CODE_CONST(bikerOff);
	
	
	//BANK_setDataBank(9);	
	VDP_printText((SCREEN_WIDTH-7)/2, 1, VDP_BG_SEG, (inrom_char *) sprite);


	bikerPtr = GET_BITMAP(&biker_ws);
	VDP_loadTile( (inrom_u16 *) GET_DATA(bikerPtr->data_offset), bikerPtr->width*bikerPtr->height, USER_TILE_INDEX);
	VDP_setPalIndexes(PAL_MONO_12, (u8 __far *) GET_DATA(bikerPtr->pal_offset));
	
	SPRITE_init( );
	
	//use the right pal because only some of them support transparency
	spriteIndex = SPRITE_add( SPRITE(CARRET_TILE, PAL_MONO_TEXT-8, SPRITE_WINDOWS_IN, SPRITE_PRIORITY_NORMAL, NO_HFLIP, NO_VFLIP) );
	SPRITE_setPosition(spriteIndex, 8*(SCREEN_WIDTH-6)/2, 16);
	
	bikerIndex = SPRITE_addMulti(
								SPRITE(USER_TILE_INDEX, PAL_MONO_12-8, SPRITE_WINDOWS_IN, SPRITE_PRIORITY_NORMAL, NO_HFLIP, NO_VFLIP),
								4);	
	posx = 2;
	posy = 2;
	
	dirx = -1;
	diry = 1;

	
	JOY_reset();
	while (1)
	{
		wait_vsync();
		JOY_update();
		
		if (joy_current == (JOY_START | JOY_A))	break; 


		if (joy_current & JOY_X1)	posy--;
		else if (joy_current & JOY_X3)	posy++;
		
		if (joy_current & JOY_X2)	posx++;
		else if (joy_current & JOY_X4)	posx--;

		SPRITE_move(spriteIndex, dirx, diry);
		
		if (SPRITE_getY(spriteIndex) == (SCREEN_HEIGHT*8))	diry =-1;
		else if (SPRITE_getY(spriteIndex) == 0)	diry = 1;
		
		if (SPRITE_getX(spriteIndex) == (SCREEN_WIDTH*8))	dirx =-1;
		else if (SPRITE_getX(spriteIndex) == 0)	dirx = 1;
		
		
		SPRITE_setMultiPosition(bikerIndex, posx, posy, 4, offset);

		SPRITE_flush();
	}
}



////////////////////////////////////////////////////////
///////////// MENU 
////////////////////////////////////////////////////////



inrom_char vdp_title [] = "VDP Tester";
inrom_char vdp_item1 [] = "Text";
inrom_char vdp_item2 [] = "Tiles";
inrom_char vdp_item3 [] = "Sprites";
inrom_char vdp_item4 [] = "x Map";
inrom_char vdp_item5 [] = "x Scroll";

#define	MENU_SELECTOR	('*'-32)
u8 vdp_menu()
{
	u8 posx, posy;
	u8 spriteIndex;
	
	VDP_printText((SCREEN_WIDTH-17)/2, 1, VDP_BG_SEG, (inrom_char *) vdp_title);

	VDP_printText(2, 5, VDP_BG_SEG, (inrom_char *) vdp_item1);
	VDP_printText(2, 7, VDP_BG_SEG, (inrom_char *) vdp_item2);
	VDP_printText(2, 9, VDP_BG_SEG, (inrom_char *) vdp_item3);
	VDP_printText(2, 11, VDP_BG_SEG, (inrom_char *) vdp_item4);
	VDP_printText(2, 13, VDP_BG_SEG, (inrom_char *) vdp_item5);
	
	spriteIndex = SPRITE_add( SPRITE(MENU_SELECTOR, PAL_MONO_TEXT, SPRITE_WINDOWS_IN, SPRITE_PRIORITY_NORMAL, NO_HFLIP, NO_VFLIP) );
	posx = 4; //1*8;
	posy = 5*8;
	
	JOY_reset();
	while (1)
	{		
		wait_vsync();
		
		SPRITE_setPosition(spriteIndex, posx, posy);
		SPRITE_flush();
		
		JOY_update();
		if (joy_change == 0)	continue;
		
		if (joy_current == (JOY_START | JOY_A))	return 0xFF; 
		
		if (joy_pressed & JOY_A) 	return  ((posy - 5*8)/16);
		
		if (joy_pressed & JOY_X1)		posy-=16;
		else if (joy_pressed & JOY_X3)	posy+=16;
		
		if (posy < 5*8)		posy = 5*8;
		if (posy > 13*8)	posy = 13*8;
	}
}





void vdp_tester( )
{	
	while(1)
	{
		u8 choice = vdp_menu();

		VDP_clearBG();
		VDP_clearFG();
		SPRITE_reset( );

	
		switch(choice)	
		{
			case 0: 
				video_text();
				break;
				
			case 1:	
				video_tiles();
				break;
				
			case 2:	
				video_sprites();
				break;
				
			case 3:	
				//TODO
				break;
				
			case 4:	
				//TODO
				break;
				
			case 0xFF:
			default:
				return;
				break;
		}

		VDP_clearBG();
		VDP_clearFG();	
		SPRITE_reset( );
	}
}
