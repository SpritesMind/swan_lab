#include <swan.h>
#include <i86.h>

extern void input_tester();
extern void vdp_tester();
extern void serial_tester();

inrom_char menu_title [] = "Alexandria Tester";
inrom_char menu_item1 [] = "Input";
inrom_char menu_item2 [] = "VDP";
inrom_char menu_item3 [] = "X Sound";
inrom_char menu_item4 [] = "Serial";

inrom_char pressStart[] = "press START+A to end test";

extern font_rsc font_ws[];
extern bitmap_rsc spritesmind_ws;

void logo()
{
	u8 w,h, posx, posy;
	u16 ticks;
	bitmap *logoPtr;

	//spritesmind logo
	logoPtr = GET_BITMAP(&spritesmind_ws);
	w = logoPtr->width;
	h = logoPtr->height;
	VDP_loadTile( (inrom_u16 *) GET_DATA(logoPtr->data_offset), w*h, USER_TILE_INDEX);
	VDP_setPalIndexes(PAL_MONO_0, (u8 __far *) GET_DATA(logoPtr->pal_offset));
	
	posx = (SCREEN_WIDTH-w)/2;
	posy = 5;
	VDP_drawTileMap(posx+0, posy+0, w, h, VDP_BG_SEG, TILE(USER_TILE_INDEX,PAL_MONO_0,NO_HFLIP,NO_VFLIP));
	
	//to test transparency
	//VDP_setPalIndexes(PAL_MONO_4, (u8 __far *) GET_DATA(logoPtr->pal_offset));
	//VDP_setTileMap(posx+3, posy+3, w, h, VDP_FG_SEG, TILE(USER_TILE_INDEX,PAL_MONO_4,0,0));

	ticks = vcounter;
	while ( (vcounter-ticks) < VTIME(2) )
	{
		wait_vsync();
	}
}


#define	MENU_SELECTOR	('*'-32)
u8 menu_select()
{
	u8 posx, posy;
	u8 spriteIndex;
	
	VDP_printText((SCREEN_WIDTH-17)/2, 1, VDP_BG_SEG, (inrom_char *) menu_title);

	VDP_printText(2, 5, VDP_BG_SEG, (inrom_char *) menu_item1);
	VDP_printText(2, 7, VDP_BG_SEG, (inrom_char *) menu_item2);
	VDP_printText(2, 9, VDP_BG_SEG, (inrom_char *) menu_item3);
	VDP_printText(2, 11, VDP_BG_SEG, (inrom_char *) menu_item4);
	
	
	VDP_printText( (SCREEN_WIDTH-25)/2, SCREEN_HEIGHT-1, VDP_BG_SEG, (inrom_char *) pressStart);
	
	spriteIndex = SPRITE_add( SPRITE(MENU_SELECTOR, PAL_MONO_TEXT, SPRITE_WINDOWS_IN, SPRITE_PRIORITY_NORMAL, NO_HFLIP, NO_VFLIP) );
	posx = 4; //1*8;
	posy = 5*8;

	JOY_reset();
	while (1)
	{		
		wait_vsync();
		
		JOY_update();

		SPRITE_setPosition(spriteIndex, posx, posy);
		SPRITE_flush();

		if (joy_change == 0)	continue;

		//intercept this special command
		if (joy_current == (JOY_START | JOY_A))	continue; 		

		
		if (joy_pressed == JOY_A) 	return  ((posy - 5*8)/16);
		
		if (joy_pressed == JOY_X1)		posy-=16;
		else if (joy_pressed == JOY_X3)	posy+=16;
		
		if (posy < 5*8)		posy = 5*8;
		if (posy > 11*8)	posy = 11*8;
	}
}


void main(void)
{	
	VDP_setSwanResPal( );
	VDP_setFont( GET_FONT_CS(&font_ws) );
	
	
	VDP_setBackgroundColor(COLOR_BLACK);
	logo();

	VDP_clearBG();
	VDP_clearFG();
	
	
	VDP_setBackgroundColor(COLOR_WHITE);
	

	JOY_reset();
	while(1)
	{
		u8 choice = menu_select();

		VDP_clearBG();
		VDP_clearFG();
		SPRITE_reset( );
	
		switch(choice)	
		{
			case 0: 
				input_tester();
				break;
				
			case 1:	
				vdp_tester();
				break;
				
			case 2:	
				//TODO
				break;
				
			case 3:	
				serial_tester();
				break;
		}

		VDP_clearBG();
		VDP_clearFG();	
		SPRITE_reset( );
	
	}
	
	//WS will reboot and relaunch main()
}
